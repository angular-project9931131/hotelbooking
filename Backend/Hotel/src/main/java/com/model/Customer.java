package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Customer {

	@Id@GeneratedValue
	private int cId;
	private String fullName;
	private String mobile;
	private String emailId;
	private String password;
	
	public Customer() {
	}
	
	public Customer(int cId, String fullName, String mobile, String emailId, String password) {
		this.cId = cId;
		this.fullName = fullName;
		this.mobile = mobile;
		this.emailId = emailId;
		this.password = password;
	}
	public int getcId() {
		return cId;
	}
	public void setcId(int cId) {
		this.cId = cId;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "Customer [cId=" + cId + ", fullName=" + fullName + ", mobile=" + mobile + ", emailId=" + emailId
				+ ", password=" + password + "]";
	}
}
